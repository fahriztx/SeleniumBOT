# SeleniumBOT

Selenium is Library From Python to Scrapping Website.

## Requirement

This module requires Python 3 and Selenium. You can download Python 3 from [here](https://www.python.org/downloads/). 

## Installation

Installation is simple. It can be installed from pip using the following command:
```sh
$ pip install selenium
```

## Usage

```sh
$ python lazada.py
```