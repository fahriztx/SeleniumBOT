from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import datetime,time

payment = {
    "COD":{
        "text":"Bayar di Tempat"
    },
    "TF":{
        "text":"melalui bank transfer",
        "BCA":"BCA",
        "BNI":"BNI & Bank Lainnya",
        "MANDIRI":"Mandiri"
    },
    "MART":{
        "text":"Bayar di counter",
        "ALFA":"Alfamart",
        "INDO":"Indomaret"
    }
}

def Log(text):
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    print("["+str(st)+"] > "+text)

def PleaseWait(timex):
    for x in range(1,timex):
        Log("Please wait... "+str(timex-x)+"s")
        time.sleep(1)
                        
browser=webdriver.Chrome()
def login(email,passwd):
    Log("Goto Lazada.co.id Login")
    browser.get('https://member.lazada.co.id/user/login')
    Log("Login Account")
    n
    password.submit()
    time.sleep(1)
    Log("Login Success")

def clearCart(url=None):
    try:
        browser.get("https://cart.lazada.co.id/cart")
        clear = browser.find_element_by_css_selector("span.lazada-ic-Delete")
        clear.click()
        konfirm = browser.find_element_by_css_selector("button.next-btn-primary.next-btn-medium")
        konfirm.click()
        Log("Cart Cleared!")
        time.sleep(2)
        if(url != None):
            browser.get(url)
    except Exception as e:
        Log("No Item In Cart")

def buy(url,harga,pay,looping):
    loop = True
    while(loop):
        browser.get(url)
        crtchk = browser.find_element_by_id("topActionCartNumber")
        # if(crtchk.text != ''):
        #     clearCart(url)
        Log(browser.title)
        if "-" in pay:
            splt = pay.split("-")
            mode = splt[0]
            opt  = splt[1]

            if payment[mode][opt] == None:
                Log("Type Pembayaran Tidak Tersedia!!!")
                exit(0)

        else:
            mode = pay

            if payment[mode] == None:
                Log("Type Pembayaran Tidak Tersedia!!!")
                exit(0)

        try:
            button = browser.find_element_by_css_selector("button.pdp-button_size_xl")
            price  = browser.find_element_by_css_selector("span.pdp-price_size_xl")
        except:
            button  = browser
            price   = browser
            button.text = "NOT"
            price.text  = "NOT"
        if button.text != "TAMBAH KE TROLI":
            Log("Product not ready! Refresing...")
            pass
        elif price.text != harga:
            Log("Price not valid! Refresing...")
            pass
        else:
            Log("[BUYING]")
            Log("Goto "+url)
            button.click()
            Log("Add Troli")
            time.sleep(3)
            checkout = browser.find_element_by_css_selector("button.automation-checkout-order-total-button-checkout")
            checkout.click()
            Log("Checkout")
            paymentx = browser.find_element_by_css_selector("button.automation-checkout-order-total-button-button")
            paymentx.click()
            Log("Payment")
            time.sleep(5)
            for i in range(0,10):
                try:
                    cod = browser.find_element_by_css_selector("#automation-payment-method-item-"+str(i)+" div div.title")
                    if cod.text == payment[mode]["text"]:
                        if payment[mode]["text"] == "Bayar di Tempat":
                            Log("Payment Type COD!")
                            cod.click()
                            time.sleep(3)
                            beli = browser.find_element_by_css_selector("button.automation-btn-place-order")
                            beli.click()
                            Log("Success Buy "+browser.title)
                            PleaseWait(10)
                            Log("Redirecting...")
                            loop = looping
                        else:
                            cod.click()
                            time.sleep(3)
                            for a in range(0,10):
                                try:
                                    beli = browser.find_element_by_css_selector("#automation-list-item-"+str(a)+" div.item-content div p")
                                    if beli.text == payment[mode][opt]:
                                        Log("Payment Type "+opt+" !")
                                        beli.click()
                                        pex = browser.find_element_by_css_selector("button.automation-btn-place-order")
                                        Log(pex.text)
                                        pex.click()
                                        loop = False
                                except Exception as e:
                                    pass
                            sleep(10)
                except Exception as e:
                    pass
        pass
try:
    login("email","password")
    buy("https://www.lazada.co.id/products/redmi-note-5-3-32gb-black-i325107650-s331484402.html","Rp2.499.000","COD",True)
except Exception as e:
    print(str(e))
except KeyboardInterrupt as k:
    Log("Cancelled by User!")

# COD
# TF-MANDIRI
# TF-BCA
# MART-ALFA
# MART-INDO

# https://www.lazada.co.id/products/redmi-note-5-4-64gb-blue-i325107667-s331484419.html
